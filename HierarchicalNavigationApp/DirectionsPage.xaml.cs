﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class DirectionsPage : ContentPage
    {
        public DirectionsPage(string addressOfSchool)
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(DirectionsPage)}: ctor");
            InitializeComponent();

            addressLabel.Text = addressOfSchool;
        }
    }
}
