﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class CollegeTuitionPage : ContentPage
    {
        public CollegeTuitionPage()
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(CollegeTuitionPage)}: ctor");
            InitializeComponent();
        }
    }
}
