﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class HierarchicalNavigationAppPage : ContentPage
    {
        

        public HierarchicalNavigationAppPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HierarchicalNavigationAppPage)}: ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }


        async void OnDirectionsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(OnDirectionsTapped)}");
            string response = await DisplayActionSheet("Overloaded Action Sheet", "cancel", null, "Address", "Maybe the Address", "It might be the Address");
            Debug.WriteLine($"User Picked:{response}");

            if (response.Equals("Address", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new DirectionsPage("333 N Twin Oaks Valley Rd, San Marcos, CA 92069"));

            }
        }

        async void OnYesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(OnYesTapped)}");

            bool usersResponse = await DisplayAlert("Free College Tuition", "Are you sure you want to come in to get this free college tuition", "Yes", "Nah I dont need money");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new CollegeTuitionPage());
            }
        }
    }
}
