﻿using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HierarchicalNavigationApp
{
    public partial class App : Application
    {
        public App()
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(App)}: ctor");
            InitializeComponent();

            MainPage = new NavigationPage(new HierarchicalNavigationAppPage());
        }

        protected override void OnStart()
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(OnStart)}: ctor");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(OnSleep)}: ctor");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"****{this.GetType().Name }.{nameof(OnResume)}: ctor");
            // Handle when your app resumes
        }
    }
}
